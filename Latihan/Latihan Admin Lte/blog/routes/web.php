<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('contoh', 'App\Http\Controllers\HomeController@forms');

Route::get('home', 'App\Http\Controllers\HomeController@home');

Route::get('register', 'App\Http\Controllers\AuthController@register');

Route::get('welcome', 'App\Http\Controllers\AuthController@welcome');

Route::post('welcome', 'App\Http\Controllers\AuthController@welcome_post');

Route::get('/master', function () {
    return view('Lte.master');
});

Route::get('/items', function () {
    return view('items.index');
});

Route::get('/create', function () {
    return view('items.create');
});

Route::get('/table', function () {
    return view('items.table');
});

Route::get('/data-table', function () {
    return view('items.data_table');
});

/* Route::get('/home', function () {
    return view('latihan.index');
}); 

Route::get('/register', function () {
    return view('latihan.form');
});

Route::get('/welcome', function () {
    return view('latihan.sambut');
}); */
