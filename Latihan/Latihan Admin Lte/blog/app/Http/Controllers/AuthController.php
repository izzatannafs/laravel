<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('latihan.form');
    }

    public function welcome() {
        return view('latihan.sambut');
    }

    public function welcome_post(Request $request) {
        $nama = $request->nama;
        $nama_belakang = $request->nama_belakang;

        return view('latihan.sambut', compact('nama', 'nama_belakang'));
        //return "$nama";
    }
}
